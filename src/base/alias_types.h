#pragma once

#include <type_traits>

#include "common.h"

_J_NS_BEGIN

#if defined(_J_CXX_14)
  using std::enable_if_t;
#else
  template <bool B, typename T = void>
  using enable_if_t = typename std::enable_if<B, T>::type;
#endif

_J_NS_END
