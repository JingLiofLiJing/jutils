#pragma once

#include "common.h"

#if defined(_J_HAS_STRING_VIEW)
#  include <string_view>
_J_NS_BEGIN
using std::string_view;
_J_NS_END
#else
#  include "details/string_view.h"
#endif
