#pragma once

#include <iterator>
#include <string>

#include "common.h"

_J_NS_BEGIN

template <typename Traits>
class string_view_iterator {
public:
    using value_type         = typename Traits::char_type;
    using difference_type    = std::ptrdiff_t;
    using pointer            = const value_type*;
    using reference          = const value_type&;
    using iterator_category  = std::random_access_iterator_tag;

    constexpr string_view_iterator() noexcept = default;

    constexpr explicit string_view_iterator(pointer ptr) noexcept
          : ptr_(ptr) {}

    constexpr string_view_iterator(const string_view_iterator&) noexcept = default;

    ~string_view_iterator() = default;

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator& operator=(const string_view_iterator&) noexcept = default;

    constexpr reference operator*() const noexcept {
        return (*ptr_);
    }

    constexpr pointer operator->() const noexcept {
        return ptr_;
    }

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator& operator++() noexcept {
        ++ptr_;
        return (*this);
    }

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator operator++(int) noexcept {
        string_view_iterator tmp(*this);
        ++(*this);
        return tmp;
    }

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator& operator--() noexcept {
        --ptr_;
        return (*this);
    }

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator operator--(int) noexcept {
        string_view_iterator tmp(*this);
        --(*this);
        return tmp;
    }

    // Before c++20, std::swap is not constexpr.
    _J_CONSTEXPR_IF_CXX_14
    void swap(string_view_iterator& other) noexcept {
        pointer tmp = ptr_;
        ptr_ = other.ptr_;
        other.ptr_ = tmp;
    }

    constexpr bool operator==(const string_view_iterator& other) const noexcept {
        return (ptr_ == (other.ptr_));
    }

    constexpr bool operator!=(const string_view_iterator& other) const noexcept {
        return (!(operator==(other)));
    }

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator& operator+=(difference_type offset) noexcept {
        ptr_ += offset;
        return (*(this));
    }

    constexpr string_view_iterator operator+(difference_type offset) const noexcept {
        return ((string_view_iterator(*this)) += offset);
    }

    _J_CONSTEXPR_IF_CXX_14
    string_view_iterator& operator-=(difference_type offset) noexcept {
        ptr_ -= offset;
        return (*(this));
    }

    constexpr string_view_iterator operator-(difference_type offset) const noexcept {
        return ((string_view_iterator(*this)) -= offset);
    }

    constexpr difference_type operator-(const string_view_iterator& other) const noexcept {
        return (ptr_ - other.ptr_);
    }

    constexpr reference operator[](difference_type offset) const noexcept {
        return (*(operator+(offset)));
    }

    constexpr bool operator<(const string_view_iterator& other) const noexcept {
        return (ptr_ < (other.ptr_));
    }

    constexpr bool operator>(const string_view_iterator& other) const noexcept {
        return (ptr_ > (other.ptr_));
    }

    constexpr bool operator<=(const string_view_iterator& other) const noexcept {
        return (!(operator>(other)));
    }

    constexpr bool operator>=(const string_view_iterator& other) const noexcept {
        return (!(operator<(other)));
    }

private:
    pointer ptr_ = nullptr;
};

template <typename Traits>
inline constexpr void swap(string_view_iterator<Traits>& lhs,             // NOLINT
                           string_view_iterator<Traits>& rhs) noexcept {
    lhs.swap(rhs);
}

template <typename Traits>
inline constexpr string_view_iterator<Traits> operator+(typename string_view_iterator<Traits>::difference_type offset,
                                                        const string_view_iterator<Traits>& rhs) noexcept {
    return (rhs += offset);
}

template class string_view_iterator<std::char_traits<char>>;

_J_NS_END
