#pragma once

#include <string>

#include "string_view_iterator.h"

_J_NS_BEGIN

template <typename CharT,
          typename Traits = std::char_traits<CharT>>
class base_string_view {
    static_assert(std::is_same<CharT,
                               typename Traits::char_type>::value,
                  "Std requires that Traits::char_type must be the same type as CharT.");
    static_assert((!std::is_array<CharT>::value) &&
                  std::is_trivial<CharT>::value &&
                  std::is_standard_layout<CharT>::value,
                  "Char-like types must meet the non-array trivial standard-layout requirements.");

public:
    using traits_type               = Traits;
    using value_type                = CharT;
    using pointer                   = value_type*;
    using const_pointer             = const value_type*;
    using reference                 = value_type&;
    using const_reference           = const value_type&;
    using const_iterator            = string_view_iterator<traits_type>;
    using iterator                  = const_iterator;
    using const_reverse_iterator    = std::reverse_iterator<const_iterator>;
    using reverse_iterator          = const_reverse_iterator;
    using size_type                 = std::size_t;
    using difference_type           = std::ptrdiff_t;

    static constexpr size_type npos = static_cast<size_type>(-1);

    constexpr base_string_view() noexcept = default;

    constexpr base_string_view(const base_string_view&) noexcept = default;

    ~base_string_view() = default;

    constexpr base_string_view& operator=(const base_string_view&) noexcept = default;

private:
    const_pointer  data_ = nullptr;
    size_type      size_ = 0ul;
};

using string_view = base_string_view<char>;

_J_NS_END
