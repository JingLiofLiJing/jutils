#pragma once

#if defined(__GNUG__)
#  define _J_GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)
#  if _J_GCC_VERSION < 50000
#    error "GCC(G++) version must be >= (5)."
#  endif
#  define _J_COMPILER_GCC
#elif defined(__clang__)
#  define _J_CLANG_VERSION (__clang_major__ * 10000 + __clang_minor__ * 100 + __clang_patchlevel__)
#  if _J_CLANG_VERSION < 30300
#    error "Clang version must be >= (3.3)."
#  endif
#  define _J_COMPILER_CLANG
#else
#  error "Unsupported c++ compiler."
#endif
