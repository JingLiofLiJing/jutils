#pragma once

#include "compiler_utils.h"

#if defined(_J_COMPILER_GCC) || defined(_J_COMPILER_CLANG)
#  define _J_CXX_STD __cplusplus
#endif

#if _J_CXX_STD < 201103L
#  error "C++ standard must be >= (11)."
#else
#  define _J_CXX_11
#endif
#if _J_CXX_STD >= 201402L
#  define _J_CXX_14
#endif
#if _J_CXX_STD >= 201703L
#  define _J_CXX_17
#endif
#if _J_CXX_STD >= 202002L
#  define _J_CXX_20
#endif

#if defined(_J_CXX_17) && __has_include(<string_view>)
#  define _J_HAS_STRING_VIEW
#endif
