#pragma once

#define _J_BEGIN_NAMESPACE(NS) namespace NS {
#define _J_BEGIN_ANONYMOUS_NAMESPACE namespace {

#define __J_END_NAMESPACE }
#define _J_END_NAMESPACE(NS) __J_END_NAMESPACE
#define _J_END_ANONYMOUS_NAMESPACE __J_END_NAMESPACE

#define _J_NS_BEGIN _J_BEGIN_NAMESPACE(jutils)
#define _J_NS_END _J_END_NAMESPACE(jutils)
#define _J_PREFIX ::jutils
