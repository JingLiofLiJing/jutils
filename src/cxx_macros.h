#pragma once

#include "cxx_utils.h"

#if defined(_J_CXX_14)
#  define _J_CONSTEXPR_IF_CXX_14 constexpr
#else
#  define _J_CONSTEXPR_IF_CXX_14
#endif
